const ucfirst = (str) => {
    if (typeof str !== "string" || !str) return "";
    
    return str[0].toUpperCase() + str.substring(1);
}
// console.log(ucfirst("hello world"));

const capitalize = (str) => {
    if (typeof str !== "string" || !str) return "";

    return str.toLowerCase().split(" ")
    .map( (item) => {
        return ucfirst(item);
    })
    .join(" ");
 
}
// console.log(capitalize("hello world"));

const camelCase = (str) => {
    word = capitalize(str);
    return word.replace(/\s/g, "");
}
// console.log(camelCase("ToggleCase is_the coolest"));

const snake_case = (str) => {
    if (typeof str !== "string" || !str) return "";

    return str.toLowerCase().replace(/[^a-z0-9]/g, '_');

    // const words = str.toLowerCase().split(" ");
    // for (let i=0; i < words.length;) {
    //     return words.join("_");
    // }
}
// console.log(snake_case("hello World"));

const replaceChr = (str) => {
    var chrs = {
        'a':'4',
        'e':'3',
        'i':'1',
        'o':'0',
        'u':'(_)',
        'y':'7'
    };
    return chrs[str.toLowerCase()] || str;
}

const leet = (str) => {
    if (typeof str !== "string" || !str) return "";

    const replaceChr = (str) => {
        var chrs = {
            'a':'4',
            'e':'3',
            'i':'1',
            'o':'0',
            'u':'(_)',
            'y':'7'
        };
        return chrs[str.toLowerCase()] || str;
    }
    return res = str.replace(/[aeiouy]/g, replaceChr);
}
// console.log(leet("Hello World"));


function prop_access(object, path) {
    if (typeof path != "string"){
        return object;
    }
    if(typeof object != 'object' || object == null) {
        console.log(path + ' not exist');
        return;
    }
    if (path === null || path === '') {
        return object;
    }
    const props = path.split('.');
    let property = object;
    props.forEach(function (prop) {
        if(!property.hasOwnProperty(prop)) {
            console.log(path + ' not exist');
            return;
        }
        property = property[prop];
    });
    return property;
}


const verlan = (str) => {
    if (typeof str !== "string" || !str) return "";
 
    return str.split(" ")
    .map( (word) => {
        return word.split("").reverse().join("");
    })
    .join(" ");
}   
// console.log(verlan("Hello World"));

const yoda = (str) => {
    if (typeof str !== "string" || !str) return "";
    
    return words = str.split(" ").reverse().join(" ");
}
// console.log(yoda("Hello World"));

const vig = (str) => {
    if (typeof str !== "string" || !str) return "";

    while(code.length < str.length) {
        code += code;
    }
    
    let codeIndex = 0;

    return str.split('')
    .map( (char) => {
        const carCode = char.charCodeAt(0) - "a".charCodeAt(0);

        if(carCode < 0 || carCode > 25) return char;
        const codeCode = code[codeIndex++].charCodeAt(0) - "a".charCodeAt(0);

        const encryptedCode = carCode + codeCode % 26;

        return String.fromCharCode(encryptedCode + "a".charCodeAt(0));
    })
    .join('');
}
