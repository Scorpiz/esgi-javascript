const type_check_v1 = (prop, type) => {
 
    switch(typeof prop) {
        case 'object':
            if (Array.isArray(prop)) return type === "array";
            if (prop === null) return type === "null";
        default:
            return prop === type
    }
}

// console.log(type_check_v1({'prop': 1}, "object"));
// console.log(type_check_v1(['a', 1], 'array'));
// console.log(type_check_v1(function test(){}, 'function'));

const type_check_v2 = (arg1, arg2) => {
    if (arg2.enum) {
        for (i in arg2.enum) {
            if (arg2.enum.i === arg1) {
                return type_check_v1(arg1, arg2.enum.i)
            }
        }
    }

    if (arg2.type && type_check_v1(arg1, arg2.type) && !arg2.enum) {
        if (arg2.value) {
            return JSON.stringify(arg1) === JSON.stringify(arg2.value)
        }
        return true;
    }
    return false;
}

console.log(type_check_v2("foo", {type: 'string', value: 'foo'}));


